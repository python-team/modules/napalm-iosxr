Source: napalm-iosxr
Section: python
Priority: optional
Maintainer: Vincent Bernat <bernat@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends: debhelper-compat (= 9), dh-python,
               python-all,
               python-netaddr,
               python-setuptools,
               python-pyiosxr,
               python-napalm-base,
               python-pip
Standards-Version: 4.1.1
Homepage: https://github.com/napalm-automation/napalm-iosxr
Vcs-Git: https://salsa.debian.org/python-team/packages/napalm-iosxr.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/napalm-iosxr

Package: python-napalm-iosxr
Architecture: all
Depends: ${misc:Depends}, ${python:Depends},
Recommends: ${python:Recommends}
Suggests: ${python:Suggests}
XB-Python-Egg-Name: napalm-iosxr
Description: abstraction layer for multivendor network automation - IOS-XR support
 NAPALM (Network Automation and Programmability Abstraction Layer with
 Multivendor support) is a Python library that implements a set of
 functions to interact with different router vendor devices using a
 unified API.
 .
 NAPALM supports several methods to connect to the devices, to
 manipulate configurations or to retrieve data.
 .
 This module provides support for Cisco IOS-XR-based devices.
